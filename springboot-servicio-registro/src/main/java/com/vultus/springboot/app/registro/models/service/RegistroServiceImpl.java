package com.vultus.springboot.app.registro.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vultus.springboot.app.registro.models.dao.RegistroDao;
import com.vultus.springboot.app.registro.models.entity.Usuario;

@Service
public class RegistroServiceImpl implements IRegistroService {
	
	@Autowired
	private RegistroDao registroDao;

	@Override
	@Transactional(readOnly = true)
	public List<Usuario> findAll() {
		return (List<Usuario>) registroDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Usuario findById(Long id) {
		return registroDao.findById(id).orElse(null);
	}

}
