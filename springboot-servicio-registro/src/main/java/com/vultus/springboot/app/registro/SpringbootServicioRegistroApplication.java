package com.vultus.springboot.app.registro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootServicioRegistroApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioRegistroApplication.class, args);
	}

}
