package com.vultus.springboot.app.cuentas.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.vultus.springboot.app.cuentas.models.Banco;
import com.vultus.springboot.app.cuentas.models.Cuenta;
import com.vultus.springboot.app.cuentas.models.service.ICuentaService;

@RestController
public class cuentaController {

	@Autowired
	@Qualifier("serviceFeign")
	private ICuentaService cuentaService;
	
	@GetMapping("/listar")
	public List<Cuenta> listar(){
		return cuentaService.findAll();
	}
	
	@HystrixCommand(fallbackMethod = "metodoAlternativo")
	@GetMapping("/ver/{clave}/nombre/{alias}/dinero/{dinero}")
	public Cuenta detalle(@PathVariable Integer clave, @PathVariable String alias, @PathVariable Integer dinero) {
		return cuentaService.findById(clave, alias, dinero);
	}
	
	@GetMapping("/bancos/detalles")
	public List<Object> bancosRegistrados() {
		return cuentaService.bancosR();
	}
	
	@GetMapping("/banco/especifico/{key}")
	public Object banco(@PathVariable Integer key) {
		return cuentaService.findByInst(key);
	}
	
	public Cuenta metodoAlternativo(Integer clave, String alias, Integer dinero) {
		Cuenta cuenta = new Cuenta();
		Banco banco = new Banco();
		cuenta.setDinero(dinero);
		cuenta.setBancaria("Scotiabank");
		banco.setClave(clave);
		banco.setAlias(alias);
		cuenta.setBanco(banco);
		return cuenta;
	}
}
