package com.vultus.springboot.app.cuentas.models.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.vultus.springboot.app.cuentas.clientes.BancoClienteRest;
import com.vultus.springboot.app.cuentas.models.Cuenta;

@Service("serviceFeign")
@Primary
public class CuentaServiceFeign implements ICuentaService {
	
	@Autowired
	private BancoClienteRest clienteFeign;

	@Override
	public List<Cuenta> findAll() {
		return clienteFeign.listar().stream().map(b -> new Cuenta(b, "Bancomer", 200)).collect(Collectors.toList());
	}

	@Override
	public Cuenta findById(Integer id, String alias, Integer dinero) {
		return new Cuenta(clienteFeign.nombre(id), alias, dinero);
	}

	@Override
	public List<Object> bancosR() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object findByInst(Integer key) {
		// TODO Auto-generated method stub
		return null;
	}

}
